package _01control;

import java.util.*;

public class E2_4 {

    public static void main(String[] args) {
        Scanner number = new Scanner(System.in);

        System.out.println("Enter an integer: ");
        int first = number.nextInt();
        System.out.println("Enter another integer: ");
        int second = number.nextInt();

        System.out.println(first + second);
        System.out.println(first - second);
        System.out.println(first * second);
        System.out.println((first + second) / 2);
        System.out.println(Math.abs(first - second));
        System.out.println(Math.max(first, second));
        System.out.println(Math.min(first, second));

        number.close();
    }
}
