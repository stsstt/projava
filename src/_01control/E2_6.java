package _01control;

import java.util.Scanner;

public class E2_6 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Measurement in meters: ");
        double meter = scanner.nextDouble();

        System.out.println("In miles: " + meter*0.000621371);
        System.out.println("In feet: " + meter*3.28084);
        System.out.println("In inches: " + meter*39.3701);

        scanner.close();
    }
}
