package _01control;

import java.util.Scanner;

public class E3_14 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter month: ");
        int month = scanner.nextInt();
        System.out.println("Enter day: ");
        int day = scanner.nextInt();

        String season = "";
        if (month%3==0 && day>=21) {
            if (season=="Winter") {
                season = "Spring";
                System.out.println("Season = "+season);
            }else if (season=="Spring") {
                season = "Summer";
                System.out.println("Season = "+season);
            }else if (season=="Summer") {
                season = "Fall";
                System.out.println("Season = "+season);
            }else {
                season = "Winter";
                System.out.println("Season = "+season);
            }
        }else {
            if (month<=3) {
                season = "Winter";
                System.out.println("Season = "+season);
            }else if (month<=6) {
                season = "Spring";
                System.out.println("Season = "+season);
            }else if (month<=9) {
                season = "Summer";
                System.out.println("Season = "+season);
            }else {
                season = "Fall";
                System.out.println("Season = "+season);
            }
        }
        scanner.close();
    }

}
