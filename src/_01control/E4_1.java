package _01control;

import java.util.Scanner;

public class E4_1 {
    public static void main(String[] args) {

        //(a)
        int sumEven = 0;
        int lowE = 2, highE = 100;
        while (lowE <=highE) {
            sumEven += lowE;
            lowE += 2;
        }
        System.out.println("(a) "+sumEven);

        //(b)
        int sumSquares = 0;
        int lowS = 1, highS = 100;
        while (lowS*lowS <=highS) {
            sumSquares += lowS*lowS;
            lowS++;
        }
        System.out.println("(b) "+sumSquares);

        //(c)
        int two = 1;
        System.out.print("(c)");
        for (int i=0; i<=20; i++) {
            System.out.println("\t2^"+i+"= "+two);
            two *= 2;
        }

        //(d)
        int sumOdd = 0;
        Scanner scanner = new Scanner(System.in);
        System.out.println("Integer a: ");

        int a = scanner.nextInt();
        System.out.println("Integer b: ");
        int b = scanner.nextInt();

        while (a<=b) {
            sumOdd += a;
            a += 2;
        }

        System.out.println("(d) "+sumOdd);

        //(e)
        int sumInput = 0;
        System.out.println("Input number: ");
        int c = scanner.nextInt();
        while (c>0) {
            int dec = c % 10;
            sumInput += dec%2==1 ? dec : 0;
            c = c/10;
        }
        System.out.println("(e) "+sumInput);

        scanner.close();
    }
}
