package _01control;

import java.util.Scanner;

public class E4_17 {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Input number: ");
        int num = scanner.nextInt();
        while (num != 0) {
            int bin = num % 2;
            System.out.print(bin);
            num /= 2;
        }
        scanner.close();
    }
}
