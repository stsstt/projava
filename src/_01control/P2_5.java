package _01control;

import java.util.Scanner;

public class P2_5 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter amount in dollars: ");
        double price = scanner.nextDouble();

        int dollars = (int) price;
        int cents = (int) ((price - dollars) *100 + 0.5);
        System.out.println(cents);

        System.out.println("Price: "+dollars+" dollars and "+cents+" cents");

        scanner.close();
    }
}
