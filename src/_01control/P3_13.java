package _01control;

import java.util.Scanner;

public class P3_13 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Input integer: ");
        int integer = scanner.nextInt();

        String roman = "";
        //invalid input
        if (integer>3999 || integer<1)
            System.out.println("Enter a number between 1 and 3999");
        else {
            //decimal using I,V,X
            int dec = integer%10;
            switch(dec) {
                case 0 :	break;
                case 1 :	roman = "I";	break;
                case 2 :	roman = "II";	break;
                case 3 :	roman = "III";	break;
                case 4 :	roman = "IV";	break;
                case 5 :	roman = "V";	break;
                case 6 :	roman = "VI";	break;
                case 7 :	roman = "VII";	break;
                case 8 :	roman = "VIII";	break;
                case 9 :	roman = "IX";	break;
            }
            //tenth using X,L,C
            integer /= 10;
            dec = integer%10;
            switch(dec) {
                case 0 :	break;
                case 1 :	roman = "X" + roman;	break;
                case 2 :	roman = "XX" + roman;	break;
                case 3 :	roman = "XXX" + roman;	break;
                case 4 :	roman = "XL" + roman;	break;
                case 5 :	roman = "L" + roman;	break;
                case 6 :	roman = "LX" + roman;	break;
                case 7 :	roman = "LXX" + roman;	break;
                case 8 :	roman = "LXXX" + roman;	break;
                case 9 :	roman = "XC" + roman;	break;
            }
            //hundredth using C,D,M
            integer /= 10;
            dec = integer%10;
            switch(dec) {
                case 0 :	break;
                case 1 :	roman = "C" + roman;	break;
                case 2 :	roman = "CC" + roman;	break;
                case 3 :	roman = "CCC" + roman;	break;
                case 4 :	roman = "CD" + roman;	break;
                case 5 :	roman = "D" + roman;	break;
                case 6 :	roman = "DC" + roman;	break;
                case 7 :	roman = "DCC" + roman;	break;
                case 8 :	roman = "DCCC" + roman;	break;
                case 9 :	roman = "CM" + roman;	break;
            }
            //thousandth using M
            integer /= 10;
            dec = integer%10;
            switch(dec) {
                case 0 :	break;
                case 1 :	roman = "M" + roman;	break;
                case 2 :	roman = "MM" + roman;	break;
                case 3 :	roman = "MMM" + roman;	break;
            }
        }
        //	1978 --> M CM LXX VIII
        System.out.println("Convert to Roman numerals: " + roman);
        scanner.close();
    }
}
