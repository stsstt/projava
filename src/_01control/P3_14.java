package _01control;

import java.util.Scanner;

public class P3_14 {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Input number: ");
        int year = scanner.nextInt();

		/*	correction after 1582
			divisible by 4 YES but by 100 NO
			divisible by 400 YES
		*/
        System.out.print("Year "+year+" is leap year: ");
        boolean bool = false;

        if (year<=1582)
            bool = false;
        else {
            if (year%4==0)
                bool = true;
            if (year%100==0)
                bool = false;
            if (year%400==0)
                bool = true;
        }
        System.out.println(bool);
        scanner.close();
    }
}
