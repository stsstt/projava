package _01control;

import java.util.Scanner;

public class P3_7 {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter income: ");
        double income = scanner.nextDouble();
        double tax = 0;

        double one = 0.01 * 50000;
        double two = 0.02 * 75000;
        double three = 0.03 * 100000;
        double four = 0.04 * 250000;
        double five = 0.05 * 500000;

        if (income<=50000)	//0~50000
            tax = 0.01*income;
        else if (income<=75000)	//50000~75000
            tax = one + 0.02*(income-50000);
        else if (income<=100000)	//75000~100000
            tax = one + two + 0.03*(income-75000);
        else if (income<=250000)	//100000~250000
            tax = one + two + three + 0.04*(income-100000);
        else if (income<=500000)	//250000~500000
            tax = one + two + three + four + 0.05*(income-250000);
        else	//500000~
            tax = one + two + three + four + five + 0.06*(income-500000);

        System.out.println("Income: "+income+", Tax: "+tax+" (USD)");

        scanner.close();
    }
}