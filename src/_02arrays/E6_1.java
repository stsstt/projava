package _02arrays;

import java.util.*;

public class E6_1 {

	public static void main(String[] args) {
		Random rand = new Random();
		int[] A = new int[10];
		for (int i=0; i<A.length; i++)
			A[i] = (int)(100*rand.nextDouble());
		
		//Original array
		System.out.print("Array:\t");
		for (int a : A)	System.out.print(a+" ");
		
		//Elements at even index
		System.out.print("\nEven index:\t");
		for (int i=0; i<A.length; i+=2)	System.out.print(A[i]+" ");
		
		//Every even element
		System.out.print("\nEven elements:\t");
		for (int i=0; i<A.length; i++)	System.out.print(A[i]%2==0 ? A[i]+" " : "");
		
		//First and last element
		System.out.print("\nFirst: "+A[0]+", Last: "+A[9]);
	}
}
