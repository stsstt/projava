package _02arrays;

import java.util.*;

public class E6_12 {

	public static void main(String[] args) {
		int[] array = new int[20];
		Random rand = new Random();
		
		//insert random numbers in array of length 20
		System.out.println("Array: ");
		for (int i=0; i<array.length; i++) {
			array[i] = (int)(100*rand.nextDouble());	//change the range to 0~99 and cast to integer 
			System.out.print(array[i]+" ");
		}
		
		
		System.out.println("\nSorted Array: ");
		//sort the array from smallest to largest
		Arrays.sort(array);
		for (int i=0; i<array.length; i++) {
			System.out.print(array[i]+" ");
		}
	}
}
