package _02arrays;

import java.util.*;

public class E6_16 {

	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		System.out.println("Enter five numbers:");

		//put the five numbers into array A
		int[] A = new int[5];
		for (int i=0; i<5; i++)
			A[i] = input.nextInt();
		
		//rearrange the elements by size in order of 4-1-2-3-5, 1 being the largest
		Arrays.sort(A);	//ordered by size i.e. 5-4-3-2-1
		int five = A[0];
		int four = A[1];
		int three = A[2];
		int two = A[3];
		int one	= A[4];
		
		//make swap positions as follows, make the max(A[1]) 40 and increment the others proportionately
		A[0] = four*40/one;
		A[1] = 40;
		A[2] = two*40/one;
		A[3] = three*40/one;
		A[4] = five*40/one;
		
		int diff1 = A[1]-A[2];
		while (diff1-->0)
			System.out.println(" *");
		
		int diff2 = A[2]-A[3];
		while (diff2-->0) 
			System.out.println(" **");
		
		int diff3 = A[3]-A[0];
		while (diff3-->0)
			System.out.println(" ***");
		
		int diff4 = A[0]-A[4];
		while (diff4-->0)
			System.out.println("****");
		
		int diff5 = A[4];
		while (diff5-->0)
			System.out.println("*****");
		
	}
}
