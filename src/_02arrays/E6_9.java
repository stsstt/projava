package _02arrays;

import java.util.*;

public class E6_9 {

	public static boolean sameSet(int[] a, int[] b) {
		return isSame(a,b) || isSame(reverse(a),b);	//check if a and b are the same either directions
	}
	 
	public static boolean isSame(int[] a, int[] b) {
		Arrays.sort(a);
		Arrays.sort(b);
		int i=0, j=0;
		while (i<a.length && j<b.length) {
			while (i>0 && a[i]==a[i-1])	i++;	//skip duplicate elements in a
			while (j>0 && b[j]==b[j-1])	j++;		//and in b
			if (a[i]!=b[j])	//if there is a single mismatch, return false
				return false;
			i++;	//go to the next element in a
			j++;	//go to the next element in b
		}
		return true;	//true if passes through all the loop without returning false
	}
	
	//return reversed array a
	public static int[] reverse(int[] a) {
		int j = a.length-1;
		for (int i=0; i<a.length/2; i++) {	//only go up to half the length
			int temp = a[i];	//exchange elements at both ends and meet in the middle
			a[i] = a[j];
			a[j] = temp;
			j--;
		}
		return a;
	}
}
