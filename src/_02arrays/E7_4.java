package _02arrays;

import java.io.*;
import java.util.*;

public class E7_4 {
	
	public static void main(String[] args) {
	
		//Get user input for file name
		Scanner input = new Scanner(System.in);
		System.out.println("Enter file name: ");
		File file = new File(input.next());
		
		Scanner sc = null;
		input.close();
		
		//check if input file exists
		try {
			sc = new Scanner(file);
		} catch (FileNotFoundException e) {
			System.out.println("File not found");	//Exception if file not found
		}
		
		//print line by line in file
		int line = 1;
		while (sc.hasNext()) {
			System.out.println("/* "+line+" */ "+sc.nextLine());
			line++;
		}
		sc.close();
	}
}
