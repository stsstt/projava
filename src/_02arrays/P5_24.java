package _02arrays;

import java.util.*;

public class P5_24 {//P5.24 Roman numerals

	public static int romanToDecimal(String str) {
		int total = 0;
		char[] yr = new char[str.length()];	//create an array yr containing each roman numerals in str
		for (int i=0; i<yr.length; i++)
            switch(str.charAt(i)) {
                case 'I'	: yr[i] = 1;	break;
                case 'V'	: yr[i] = 5;	break;
                case 'X'	: yr[i] = 10;	break;
                case 'L'	: yr[i] = 50;	break;
                case 'C'	: yr[i] = 100;	break;
                case 'D'	: yr[i] = 500;	break;
                case 'M'	: yr[i] = 1000;	break;
            }
		
		while (yr.length>0) {	//
			if (yr.length==1 || yr[0]>=yr[1]) {	//if larger value comes before smaller value, add to total in order
				total += yr[0];
				yr = Arrays.copyOfRange(yr, 1, yr.length);	//discard the first element in y
			}else {	//otherwise if a smaller value comes before a larger value, it calls for a subtraction
				total += yr[1]-yr[0];
				yr = Arrays.copyOfRange(yr, 2, yr.length); //discard the first two elements in y
			}
		}
		return total;
	}
}
