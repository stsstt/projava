package _02arrays;

public class P5_8 {
	public static boolean isLeapYear(int year) {
		if (year<=1582)	//has to be greater than 1582 for leap year condition to apply
			return false;
		if (year%4==0) {	//leap year if divisible by 4
			if (year%100==0)	//except when divisible by 100
				if (year%400==0)	//but still leap year if divisible by 400
					return true;
			return false;
		}
		return true;
	}
}
