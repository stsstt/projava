package _02arrays;

import java.util.*;
import java.io.*;

public class P7_5 {

	public static void main(String[] args) {
		//get user input for file name
		Scanner input = new Scanner(System.in);
		System.out.println("Enter file name: ");
		File file = new File(input.next());
		
		Scanner sc = null;
		input.close();
		
		//check if file with that name exists
		try {
			sc = new Scanner(file);
		} catch (FileNotFoundException e) {
			System.out.println("File not found");	//Exception if file not found
		}
		
		int row=1, col=5;	//give values for row and column
		
		int numberOfRows=0;	//number of rows
		int numberOfFields=0;	//number of fields at row 2
		String field = "";	//field at row 2 and column 3
		
		while (sc.hasNext()) {
			String s = sc.nextLine();
			numberOfRows++;	//increment number of rows until all lines have been read
			if (numberOfRows==row) {
				numberOfFields = s.split(",").length;	//number of fields = number of elements divided by ","
				field = s.split(",")[col-1];	//field of interest = col-1-th element at given row
			}
		}
		System.out.println("numberOfRows: "+numberOfRows);
		System.out.println("numberOfFields: "+numberOfFields);
		System.out.println("field: "+field);
		sc.close();
	}
}
