R5.6 Parameters and return values
The difference between an argument and a return value is that an argument is passed
into a method, whereas a return value is the value returned from it.

You can have n number of arguments, whereas you can only have one return value. By varying either the number and/or type of arguments,
you change the method's signature, which means you may overload a method. Varying the return value only does not change the
method's signature and therefore does not qualify the method for overloading.

Since Java5, you can even specify a variable number of arguments aka varargs which looks something like this:
 private String[] someMethod(int nGrade, String... strNickNames) {//implementation};
Notice that the varargs parameter "String... strNickNames" must be listed last.  Calling this method would look something like this:
 String[] strReturnValues = someMethod(99, "Jim", "Jamie", "James", "Jamey", "Jimmy", "Jimbo");

R5.8 Pseudocode
    Phone(number)
    Create an empty string s
    for i=1 to 4
        s = s + number.charAt(i)
    for i=5 to 11
        switch(number.charAt(i))
            case (1 or 0)   :   s = s + 1 or 0
            case(a or b or c)   :   s = s + 2
            case(d or e or f)   :   s = s + 3
            case(g or h or i)   :   s = s + 4
            case(j or k or l)   :   s = s + 5
            case(m or n or o)   :   s = s + 6
            case(p or q or r or s)   :   s = s + 7
            case(t or u or v)   :   s = s + 8
            case(w or x or y or z)   :   s = s + 9
    return s

R5.10 Variable scope
Method f(i) returns the greatest integer that is less than the square root of i.
Method g(a)  returns the sum of all f(a)'s where a ranges from 0 to a.
The main method will print the sum of g(a) and a. In this case, a=10 so the main method will print
 (0 + 0 + 1 + 1 + 1 + 2 + 2 + 2 + 2 + 2 + 3) + 10 = 26
In method f(i), scope of variable n is the entire method.
In method g(a), b is also the entire method while n is only within the for loop.

R5.14 Pass by value versus pass by reference
Because in Java it is not possible for a method to change the contents of a variable passed as an argument.

R6.3 Considering loops
a/ 25
b/ 13
c/ 12
d/ ArrayIndexOutOfBoundsException
e/ 11
f/ 25
g/ 12
h/ -1

R6.10 Enhanced for-loop
a/  for (float f : values) { total = total + f; }
b/  List<Float> valuesf = new ArrayList<>();
    for (Float f : values)  valuesf.add(f);
    valuesf.remove(0);  //remove the element at index 0
    float total = 0;
    for (Float f : valuesf) { total += f; }
c/  int i = 0;
    for (float f : values)
    {
        if (f == target) { return i; }
        i++;
    }

R6.23 Computing runs
Given int array A
max = Integer.MIN_VALUE
for i=0 to A.length-1
    count = 1
    while i<A.length && A[i]==A[i+1]
        count = count + 1
    max = Math.max(max, count);
return max

R6.29 Multi-dimensional arrays
1/  Elements are already initialized to zero. Nothing to do here
2/  for (int i=0; i<rows; i++)
        for (int j=0; j<columns; j++)
            if ((i+j)%2==0)
                values[i][j]=1;
3/  for (int j=0; j<columns; j++) {
        values[0][j]=1;
        values[values.length-1][j]=1;
    }
4/  int sum = 0;
    for (int i=0; i<rows; i++)
        for (int j=0; j<columns; j++)
            sum += values[i][j];
    return sum;
5/  for (int i=0; i<rows; i++)
        for (int j=0; j<columns; j++)
            System.out.print(values[i][j]);
        System.out.println();

R6.34 Understanding arrays
a/ True
b/ True
c/ False
d/ True
e/ False
f/ True

R7.1 Exceptions
You get a FileNotFoundException when trying to read a file that doesn't exist. I get a file with length 0 when I try to open a file for writing that doesn't exist.

R7.6 Throwing and catching
Throwing exception is signaling an exceptional condition. It's a flexible mechanism for passing control from the point of error detection to a handler that can deal with the error.
Catching exception is handling that exception and enabling the program to proceed. Try/catch statement is placed where the program is capable of handling the exceptional condition.

R7.7 Checked versus unchecked
Checked exception is an exception that is checked by complier during compilation, for example, FileNotFoundException. It requires a throw statement.
Unchecked exception is an exception not checked at compilation, for example, NullPointerException. It should be handled by the programmer.

R7.8 Exceptions philosophy in Java
Because IndexOutOfBoundsException is an unchecked exception.

R7.11 What is an exception object
It can print a message and/or details about the exception and ask programmer to provide a correct input.

R7.15 Scanner exceptions. Explain why these are either checked or unchecked.
Examples of possible exceptions are InputMismatchException, IllegalStateException and NumberFormatException. These are checked exceptions.