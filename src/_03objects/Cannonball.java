package P8_19;

import java.util.Scanner;

public class Cannonball {
	
	private static final double gravAcc = -9.81;	// m/s^2
	private double x, y;	// m
	private double x_vel, y_vel;	// m/s
	
	public Cannonball(double x) {
		this.x = x;
		this.y = 0;
		this.x_vel = 0;
		this.y_vel = 0;
	}

	public void move(double sec) {
		
		this.x += x_vel * sec;
		this.y += y_vel * sec;
		this.y_vel += gravAcc * sec;
	}
	
	public double getX() {
		return this.x;
	}
	
	public double getY() {
		return this.y;
	}
		
	public void shoot(double angle, double velocity) {
		x_vel = velocity * Math.cos(Math.toRadians(angle));
		y_vel = velocity * Math.sin(Math.toRadians(angle));
		this.move(0.1);
		while (this.getY()>0) {
			this.move(0.1);
			System.out.println("X:"+this.getX()+",Y:"+this.getY());
		}
	}
	
	public static void main(String[] args) {
		Cannonball cb = new Cannonball(40);
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter angle:");
		int angle = sc.nextInt();
		System.out.println("degrees");
		System.out.println("Enter velocity (m/s):");
		int velocity = sc.nextInt();
		System.out.println("m/s");
		cb.shoot(angle,velocity);
		sc.close();
	}
}
