package P8_6;

public class Car {

	private int efficiency;
	private int fuelLevel;
	
	public Car(int e) {
		this.efficiency = e;
		this.fuelLevel = 0;
	}
	
	public void addGas(int gas) {
		this.fuelLevel += gas;
	}
	
	public void drive(int miles) {
		//remaining fuel = current fuel - distance driven/efficiency
		this.fuelLevel = this.getGasLevel() - miles/this.efficiency;
	}
	
	public int getGasLevel() {
		return this.fuelLevel;
	}
	
	public static void main(String[] args) {
		Car car = new Car(50);
		car.addGas(20);	//fuelLevel = 20
		car.drive(100);	//used 100/50 = 2 gallons of fuel
		
		System.out.println(car.getGasLevel());
	}
}
