package P8_7;

public class ComboLock {

	public static final int total = 40;	//max dial set to 40
	private int secret1;
	private int secret2;
	private int secret3;
	private int current;
	private boolean firstAttempt, secondAttempt, thirdAttempt;
	
	public ComboLock(int secret1, int secret2, int secret3) {
		if (secret1<=0 || secret1>=39 || secret2<=0 || secret2>=39 || secret3<=0 || secret3>=39)
			System.out.println("numbers should be between 0 and 39");
		this.secret1 = secret1;
		this.secret2 = secret2;
		this.secret3 = secret3;
		this.current = 0;	//current dial set to 0
		this.firstAttempt = this.secondAttempt = this.thirdAttempt = false;	//attemps initialized to false; 
	}
	
	public void reset() {
		current = 0;
	}
	public void turnLeft(int ticks) {
		if (current-ticks>=0)
			current =- ticks;
		else
			current += total - ticks;
		if (firstAttempt && current==secret2)
			secondAttempt = true;	//turned left to the second number correctly after getting the first number correctly 
	}
	public void turnRight(int ticks) {
		if (current+ticks<=40)
			current += ticks;
		else
			current = (current + ticks) % total;
		if (!firstAttempt) {
			if (current==secret1)
				firstAttempt = true;	//guessed the first number correctly
		}else {
			if (secondAttempt && current==secret3)
				thirdAttempt = true;	//if first and second attempts are successful, make third also a success 
		}
	}
	public boolean open() {
		return firstAttempt && secondAttempt && thirdAttempt;
	}
	
	public static void main(String[] args) {
		ComboLock cl = new ComboLock(13,33,27);
		cl.turnRight(13);
		System.out.println(cl.current);
		cl.turnLeft(20);
		System.out.println(cl.current);
		cl.turnRight(34);
		System.out.println(cl.current);
		System.out.println("Lock is opened: "+cl.open());
		cl.reset();
		System.out.println("current dial at: "+cl.current);
	}
}
