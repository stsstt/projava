package P8_14;

public class Country {

	private String country;
	private int population;
	private int area;
	
	public Country(String c, int p, int a) {
		this.country = c;
		this.population = p;
		this.area = a;
	}
	
	public String getName() {
		return this.country;
	}
	
	public int getPop() {
		return this.population;
	}
	
	public int getArea() {
		return this.area;
	}
	
	public double getDen() {
		return (double)population/area;
	}
	
	public static void main(String[] args) {
		Country[] countries = {	new Country("Monaco", 100, 60),
								new Country("New Zealand",900,300),
								new Country("Jamaica",250,70),
								new Country("Holland",400,180),
								new Country("Spain",500,400),
								new Country("Laos",200,200)	};
		
		//to replace with names of countries with largest area, population, density
		String maxArea = "", maxPop = "", maxDen = "";	
		
		//Country with the largest area
		int area = Integer.MIN_VALUE;	//keep track of largest area so far
		for (int i=0; i<countries.length; i++) {
			if (countries[i].getArea() > area) {
				area = countries[i].getArea();	//set this country's area as the largest so far
				maxArea = countries[i].getName();
			}
		}
		System.out.println("Country with the largest area: "+maxArea);

		//Country with the largest population
		int pop = Integer.MIN_VALUE;	//keep track of largest population so far
		for (int i=0; i<countries.length; i++) {
			if (countries[i].getPop() > pop) {
				pop = countries[i].getPop();	//set this country's population as the largest so far
				maxPop = countries[i].getName();
			}
		}
		System.out.println("Country with the largest population: "+maxPop);

		//Country with the largest population density
		double den = Integer.MIN_VALUE;	//keep track of largest population density so far
		for (int i=0; i<countries.length; i++) {
			if (countries[i].getDen() > den) {
				den = countries[i].getDen();	//set this country's area as the largest so far
				maxDen = countries[i].getName();
			}
		}
		System.out.println("Country with the largest population density: "+maxDen);

	}
}
