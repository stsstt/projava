package P8_16;

import java.util.*;

public class Mailbox {

	private ArrayList<Message> emails;
	
	public Mailbox(ArrayList<Message> m) {
		this.emails = m;
	}
	
	public void addMessage(Message m) {
		this.emails.add(m);
	}

	public Message getMessage(int i) {
		return this.emails.get(i);
	}
	
	public void removeMessage(int i) {
		System.out.println("Message removed(\n"+getMessage(i)+")\n");
		this.emails.remove(i);
	}
	
	public static void main(String[] args) {
		ArrayList<Message> mails = new ArrayList<>();
		mails.add(new Message("Harry","Sally"));
		mails.get(0).append("Coffee at Philz at 4pm tomorrow\n");
		mails.add(new Message("Mr.Smith","Mrs.Smith"));
		mails.get(1).append("Coffee at Starbucks at 5pm today\n");
		mails.add(new Message("elephant","giraffe"));
		mails.get(2).append("Coffee on campus at 7pm on Friday\n");
		mails.add(new Message("Barack","Michelle"));
		mails.get(3).append("Coffee at my office next week\n");
		mails.add(new Message("Ebony","Ivory"));
		mails.get(4).append("Coffee at Blue Bottle at 6pm on Saturday\n");

		Mailbox mb = new Mailbox(mails);
		
		System.out.println(mb.getMessage(3));
		mb.removeMessage(3);
		System.out.println(mb.getMessage(3));
		mb.addMessage(new Message("Will","Grace"));
		mb.getMessage(4).append("Coffee at Central Perk this weekend\n");
		System.out.println("Message added(\n"+mb.getMessage(4)+")\n");
	}	
}
