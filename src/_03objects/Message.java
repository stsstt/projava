package P8_16;

public class Message {
	
	private String recipient;
	private String sender;
	private String text;
	
	public Message(String recipient, String sender) {
		this.recipient = recipient;
		this.sender = sender;
		this.text = "";
	}
	
	public void append(String line) {
		text += line;
	}
	
	public String toString() {
		return "From: "+recipient+"\nTo: "+sender+"\n"+text;
	}

	public static void main(String[] args) {
		Message m = new Message("Harry","Sally");
		m.append("Coffee at Philz at 4pm tomorrow");
		System.out.println(m.toString());
	}
}
