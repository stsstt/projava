package P8_1;

public class Microwave {

	private int time;
	private int level;

	public Microwave(int time, int level) {
		this.time = time;
		this.level = level;
	}
	
	public void increaseTime() {
		time += 30;
	}
	
	public void switchLevel() {
		level = level==1 ? 2 : 1; 
	}
	
	public void start() {
		System.out.println("Cooking for "+time+" seconds at level "+level);
	}
	
	public void reset() {
		time = 0;
	}
	
	public static void main(String[] args) {
		Microwave mw = new Microwave(10,2);
		mw.switchLevel();
		mw.increaseTime();
		mw.start();
		mw.reset();
		mw.start();
	}

}
