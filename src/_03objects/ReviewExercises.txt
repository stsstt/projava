R8.1 Encapsulation
Encapsulation is the act of providing a public interface and hiding the implementation details. It enables changes in the implementation without affecting users of a class.


R8.4 Public interface
Public interface is a collection of methods through which the objects of the class can be manipulated. Public interface of a class consists of all methods that a user of the class may want to apply to its objects. While the method declarations and comments make up the public interface of a class, the data and method bodies make up the private implementation of the class.


R8.7 Instance versus static
Instance method is a method invoked on an object. Every instance method has exactly one implicit parameter and zero or more explicit parameters. Static method carrious out some work but is not invoked on an object. A static method has no implicit parameter.


R8.8 Mutator and accessor
A mutator method changes the object on which it operates. An accessor method doesn't change the object and instead queries the object for some information.


R8.9 Implicit parameter
Implicit parameter is the object on which a method is invoked. It is not actually written in the method declaration, hence the name. Explicit parameter is a parameter of a method other than the object on which the method is invoked and is stated in the method declaration.


R8.10 Implicit parameter
Instance method can have exactly one implicit parameter and zero or more explicit parameters. A static method can have none.


R8.12 Constructors
Classes can have more than one constructor. There is no class with no constructor. If there are multiple constructors, the compiler picks the constructor that matches the construction arguments.


R8.16 Instance variables
A private instance variable can only be accessed by the methods of its own class.


R8.19 The this reference
The this reference refers to the implicit parameter in a method. It is used to make it clear which variables are instance variables and not local variables. 


R8.20 Zero, null, false, empty String
The number zero references the int variable whose value is equal to zero. The null references references no object at all. Value false references the boolean variable whose value is false. Empty string references the String variable whose size is zero.