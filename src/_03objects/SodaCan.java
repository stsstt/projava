package P8_5;

public class SodaCan {

	private int height;
	private int radius;
	
	public SodaCan(int h, int r) {
		this.height = h;
		this.radius = r;
	}
	
	public double getSurfaceArea() {
		return Math.PI*this.radius*this.radius*2 + 2*Math.PI*this.radius*this.height;
	}
	
	public double getVolume() {
		return Math.PI*this.radius*this.radius*this.height;
	}
	
	public static void main(String[] args) {
		SodaCan sprite = new SodaCan(5,1);
		System.out.println(sprite.getSurfaceArea());
		System.out.println(sprite.getVolume());
	}
}
