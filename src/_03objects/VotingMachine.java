package P8_8;

public class VotingMachine {

	private int democrat;
	private int republican;
	
	public VotingMachine () {
		//tallies for both parties initialized to zero;
		this.democrat = 0;
		this.republican = 0;
	}
	public void clear() {
		//set tallies for both parties to zero
		this.democrat = 0;
		this.republican = 0;
	}
	public void voteDem() {
		democrat++;
	}
	public void voteRep() {
		republican++;
	}
	public int getTally() {
		return this.democrat+this.republican;
	}
	
	public static void main(String[] args) {
		VotingMachine vm = new VotingMachine();
		vm.clear();
		vm.voteDem();
		vm.voteDem();
		vm.voteDem();
		vm.voteRep();
		vm.voteRep();
		vm.voteDem();
		vm.voteRep();
		vm.voteRep();
		System.out.println("Democrat:"+vm.democrat+", Republican:"+vm.republican);
		System.out.println(vm.getTally());
	}
}
