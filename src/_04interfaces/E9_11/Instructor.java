package E9_11;

public class Instructor extends Person{
	
	long salary;
	
	public Instructor(String name, int birthYear, long salary) {
		super(name, birthYear);
		this.salary = salary;
	}
	//getter
	public long getSalary() {
		return this.salary;
	}
	//setter
	public void setSalary(long salary) {
		this.salary = salary;
	}
	//overrides Person's toString method
	public String toString() {
		return name + ", " + birthYear + " / Instructor / Salary: " + salary;
	}

}