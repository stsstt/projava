package E9_11;

public class Person {
	
	String name;
	int birthYear;
	
	public Person(String name, int birthYear) {
		this.name = name;
		this.birthYear = birthYear;
	}
	
	//getters
	public String getName() {
		return name;
	}
	public int getBirthYear() {
		return birthYear;
	}
	
	//setters
	public void setName(String name) {
		this.name = name;
	}
	public void setBirthYear(int year) {
		this.birthYear = year;
	}
	
	public String toString() {
		return name + ", " + birthYear;
	}
}