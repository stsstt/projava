package E9_11;

public class Student extends Person {

	String major;
	
	public Student(String name, int birthYear, String major) {
		super(name, birthYear);
		this.major = major;
	}
	
	//getter
	public String getMajor() {
		return this.major;
	}
	//setter
	public void setMajor(String major) {
		this.major = major;
	}
	//overrides Person's toString method
	public String toString() {
		return name + ", " + birthYear + " / Student / Major: " + major;
	}


}