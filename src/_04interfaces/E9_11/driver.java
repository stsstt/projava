package E9_11;

public class driver {

	public static void main(String[] args) {
		Person p1 = new Person("James Cameron",1992);
		Person p2 = new Person("Noel Leon",1962);
		Instructor i1 = new Instructor("James Cameron",1992,160000);
		Instructor i2 = new Instructor("Noel Leon",1962,90000);
		Student s1 = new Student("James Cameron",1992,"Music History");
		Student s2 = new Student("Noel Leon",1962,"Art History");
		
		p2.setBirthYear(1975);
		i2.setSalary(70000);
		s2.setMajor("Law");
		System.out.println(p1);
		System.out.println(p2);
		System.out.println(i1);
		System.out.println(i2);
		System.out.println(s1);
		System.out.println(s2);
	}

}
