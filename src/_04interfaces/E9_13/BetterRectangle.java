package E9_13;

import java.awt.*;

public class BetterRectangle extends Rectangle {
	
	public BetterRectangle(int x, int y, int width, int height) {
		setLocation(x, y);
		setSize(width, height);
	}
	
	public double getArea() {
		return super.getHeight() * super.getWidth();
	}
	
	public double getPerimeter() {
		return super.getHeight()*2 + super.getWidth()*2;
	}

}