package E9_8;

public class BasicAccount extends BankAccount {
	
	public BasicAccount(double balance) {
		super(balance);
	}
	
	public void withdraw(double amount) {
		if (super.getBalance() < amount)	//balance is less than amount
			balance = 0;	//withdraw all of what is left
		else
			super.withdraw(amount);
	}
	
}