package P9_1;

public class Clock {
	
	final static String time = java.time.LocalTime.now().toString();
	
	//time at current location
	public static String getHours() {
		return time.split(":")[0];
	}
	
	public static String getMinutes() {
		return time.split(":")[1];
	}
	
	public static String getTime() {
		return getHours() + ":" + getMinutes();
	}
	
	public static String WorldClock(int offset) {
		return (Integer.parseInt(getHours())+offset)% 24 + ":" + getMinutes() + ":" + time.split(":")[2];
	}

	public static void main(String[] args) {
		System.out.println("Current local time is " +time);
		System.out.println("Hour:"+getHours());
		System.out.println("Minute:"+getMinutes());
		System.out.println("Current time "+getTime());
		System.out.println(WorldClock(3));
	}

}