package P9_11;

public class ParallelResonantCircuit extends ResonantCircuit {
	
	public ParallelResonantCircuit(double w, double b, double k) {
		super(w,b,k);
	}

	public double getR() {
		return super.getGain();
	}
	
	public double getC() {
		return 1/(super.getBandwidth() * this.getR());
	}
	
	public double getL() {
		return 1/(super.getFrequency() * super.getFrequency() * this.getC());
	}

	@Override
	public void display() {
		System.out.println("frequency " + super.getFrequency()+ "\nbandwidth " + super.getBandwidth() + "\ngain " + super.getGain());
		System.out.println("resistance " + this.getR() + "\ncapacitance " + this.getC() + "\ninductance " + this.getL() + "\n");
	}
}
