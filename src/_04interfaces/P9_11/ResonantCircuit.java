package P9_11;

public class ResonantCircuit {

	private double frequency;
	private double bandwidth;
	private double gain;
	
	public ResonantCircuit(double w, double b, double k) {
		this.frequency = w;
		this.bandwidth = b;
		this.gain = k;
	}
	
	//Getters
	public double getFrequency() {
		return this.frequency;
	}
	public double getBandwidth() {
		return this.bandwidth;
	}
	public double getGain() {
		return this.gain;
	}
	
	//Setters
	public void setFrequency(double f) {
		this.frequency = f;
	}
	public void setBandwidth(double b) {
		this.bandwidth = b;
	}
	public void setGain(double g) {
		this.gain = g;
	}
	
	public void display() {
		System.out.println("frequency " + frequency + "\nbandwidth " + bandwidth + "\ngain " + gain + "\n");
	}
	
}