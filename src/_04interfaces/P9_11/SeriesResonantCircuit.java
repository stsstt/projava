package P9_11;

public class SeriesResonantCircuit extends ResonantCircuit {

	public SeriesResonantCircuit(double w, double b, double k) {
		super(w,b,k);
	}
	
	public double getR() {
		return 1/super.getGain();
	}
	
	public double getL() {
		return this.getR()/super.getBandwidth();
	}
	
	public double getC() {
		return 1/(super.getFrequency() * super.getFrequency() * this.getL());
	}
	
	@Override
	public void display() {
		System.out.println("frequency " + super.getFrequency()+ "\nbandwidth " + super.getBandwidth() + "\ngain " + super.getGain());
		System.out.println("resistance " + this.getR() + "\ninductance " + this.getL() + "\ncapacitance " + this.getC() + "\n");
	}
}
