package P9_11;

public class driver {

	public static void main(String[] args) {
		ResonantCircuit rc = new ResonantCircuit(3.0,7.2,13.0);
		ResonantCircuit src = new SeriesResonantCircuit(8.7,32.1,19.5);
		ResonantCircuit prc = new ParallelResonantCircuit(5.3,3.9,10.4);
		
		rc.display();
		src.display();
		prc.display();
		
	}
}