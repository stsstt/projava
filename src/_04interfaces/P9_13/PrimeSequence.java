package P9_13;

public class PrimeSequence implements Sequence {

	private int x;
	
	public PrimeSequence(int x) {
		this.x = x;
	}
	
	public int next() {
		int nextPrime = this.x+1;
		while (!isPrime(nextPrime))
			nextPrime ++;
		return nextPrime;
	}
	
	public static boolean isPrime(int x) {
		if (x==0 || x==1)	return false;
		if (x==2)	return true;
		if (x%2==0)	return false;
		
		for (int i=3; i*i<=x; i += 2) {
			if (x % i == 0)
				return false;
		}
		return true;
	}

	public static void main(String[] args) {
		PrimeSequence x = new PrimeSequence(19);
		System.out.println(x.next());
	}
	
}
