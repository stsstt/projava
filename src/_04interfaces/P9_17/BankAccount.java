package P9_17;

public class BankAccount implements Filter, Measurable {

	private double balance;
	
	public BankAccount(double balance) {
		this.balance = balance;
	}
	
	public double getMeasure() {
		return this.balance;
	}
	
	public boolean accept(Object x) {
		BankAccount account = (BankAccount) x;
		return account.getMeasure() > 1000;		
	}
	
	public double average(BankAccount[] objects) {
	   if (objects.length == 0)
		   return 0;
	   
	   double sum = 0;	   
	   for (Object obj : objects) {
		   if (accept(obj)) {
			   BankAccount o = (BankAccount) obj;
			   sum += o.getMeasure();
		   }
	   }
	   return sum/objects.length;
	}
}
