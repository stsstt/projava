package P9_17;

public interface Filter {
	
	boolean accept(Object x);
	
}

