package P9_17;

public interface Measurable {
	
	double getMeasure();

}
