package P9_6;

public class Appointment {

	private String description;
	Date date;
	
	public Appointment(String description, Date date) {
		this.description = description;
		this.date = date;
	}
	
	public Date getDate() {
		return date;
	}
	
	public String getDesc() {
		return description;
	}
	
	public boolean occursOn(int month, int day, int year) {
		return date.getMonth()==month && date.getDay()==day && date.getYear()==year;
	}
	
	public String toString() {
		return description + " (" + date.getMonth() + "/"+ date.getDay() +"/" +date.getYear() + ")";
	}
}