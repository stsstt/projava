package P9_6;

public class Daily extends Appointment {
	
	public Daily(String description, Date date) {
		super(description, date);
	}
	
	public boolean occursOn(int month, int day, int year) {
		return true;	//appointment occurs everyday
	}
}