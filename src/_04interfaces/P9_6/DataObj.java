package P9_6;

public class DateObj implements Date{

	int month;
	int day;
	int year;
	
	DateObj(int month, int day, int year) {
		this.month = month;
		this.day = day;
		this.year = year;
	}
	
	public int getMonth() {
		return month;
	}
	public int getDay() {
		return day;
	}
	public int getYear() {
		return year;
	}
}