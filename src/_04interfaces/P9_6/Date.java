package P9_6;

public interface Date {

	int getMonth();
	int getDay();
	int getYear();
	
}