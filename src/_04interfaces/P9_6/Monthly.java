package P9_6;

public class Monthly extends Appointment {
	
	public Monthly(String description, Date date) {
		super(description, date);
	}
	
	public boolean occursOn(int month, int day, int year) {
		return super.getDate().getMonth()==month;	//matches the month
	}
}