package P9_6;

public class OneTime extends Appointment {
	
	public OneTime(String description, Date date) {
		super(description, date);
	}
	
	public boolean occursOn(int month, int day, int year) {
		return super.occursOn(month, day, year);	//must match month, day and year
	}
}