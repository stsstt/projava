package P9_6;

import java.util.*;

public class driver {

	public static void main(String[] args) {
		Appointment a = new Appointment("see dentist", new DateObj(8,4,2020));
		Appointment b = new Appointment("see physician", new DateObj(8,24,2020));
		Appointment c = new Appointment("see neurologist", new DateObj(11,11,2019));
		Appointment d = new Appointment("see dermatologist", new DateObj(12,17,2019));
		Appointment e = new Appointment("check up", new DateObj(10,31,2019));
		Appointment f = new Appointment("tooth extraction", new DateObj(1,17,2020));
		Appointment g = new Appointment("see pharmacist", new DateObj(2,3,2020));
		Appointment h = new Appointment("see nurse", new DateObj(3,28,2020));

		Appointment ad = new Daily("see dentist", new DateObj(8,4,2020));
		Appointment bd = new Daily("see physician", new DateObj(8,24,2020));
		Appointment cd = new Daily("see neurologist", new DateObj(11,11,2019));
		Appointment dd = new Daily("see dermatologist", new DateObj(12,17,2019));
		Appointment ed = new Daily("check up", new DateObj(10,31,2019));
		Appointment fd = new Daily("tooth extraction", new DateObj(1,17,2020));
		Appointment gd = new Daily("see pharmacist", new DateObj(2,3,2020));
		Appointment hd = new Daily("see nurse", new DateObj(3,28,2020));

		Appointment am = new Monthly("see dentist", new DateObj(8,4,2020));
		Appointment bm = new Monthly("see physician", new DateObj(8,24,2020));
		Appointment cm = new Monthly("see neurologist", new DateObj(11,11,2019));
		Appointment dm = new Monthly("see dermatologist", new DateObj(12,17,2019));
		Appointment em = new Monthly("check up", new DateObj(10,31,2019));
		Appointment fm = new Monthly("tooth extraction", new DateObj(1,17,2020));
		Appointment gm = new Monthly("see pharmacist", new DateObj(2,3,2020));
		Appointment hm = new Monthly("see nurse", new DateObj(3,28,2020));
		
		Appointment ao = new OneTime("see dentist", new DateObj(8,4,2020));
		Appointment bo = new OneTime("see physician", new DateObj(8,24,2020));
		Appointment co = new OneTime("see neurologist", new DateObj(11,11,2019));
		Appointment Do = new OneTime("see dermatologist", new DateObj(12,17,2019));
		Appointment eo = new OneTime("check up", new DateObj(10,31,2019));
		Appointment fo = new OneTime("tooth extraction", new DateObj(1,17,2020));
		Appointment go = new OneTime("see pharmacist", new DateObj(2,3,2020));
		Appointment ho = new OneTime("see nurse", new DateObj(3,28,2020));

		Appointment[] apps = {a,b,c,d,e,f,g,h};
		Appointment[] dailys = {ad,bd,cd,dd,ed,fd,gd,hd};
		Appointment[] monthlys = {am,bm,cm,dm,em,fm,gm,hm};
		Appointment[] onetimes = {ao,bo,co,Do,eo,fo,go,ho};
		
		//Get user input for month, day, year
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter month");
		int month = sc.nextInt();
		System.out.println("Enter day");
		int day = sc.nextInt();
		System.out.println("Enter year");
		int year = sc.nextInt();
		sc.close();
		
		//Print out all appointments that occur daily/monthly/one time including this day
		for (Appointment app : apps) {
			if (app.occursOn(month, day, year))
				System.out.println("Appointment: " + app);
		}
		for (Appointment daily : dailys) {
			if (daily.occursOn(month, day, year))
				System.out.println("Daily: " + daily);
		}
		for (Appointment monthly : monthlys) {
			if (monthly.occursOn(month, day, year))
				System.out.println("Monthly: " + monthly);
		}
		for (Appointment onetime : onetimes) {
			if (onetime.occursOn(month, day, year))
				System.out.println("One time: " + onetime);
		}
	
	}

}