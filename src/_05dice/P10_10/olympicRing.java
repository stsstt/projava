package P10_10;

import java.awt.*;
import javax.swing.*;

public class olympicRing extends JFrame {

	public static final int FRAME_WIDTH = 300;
	public static final int FRAME_HEIGHT = 200;
	public static final int RING_RADIUS = 60;
	public static final int INNER_RADIUS = 50;
	
	private JComponent olympicComponent;

    public olympicRing() {
    	this.olympicComponent = new OlympicComponent();
        this.add(this.olympicComponent);
        this.setSize(FRAME_WIDTH, FRAME_HEIGHT);
    }

    public class OlympicComponent extends JComponent {
        public void paintComponent(Graphics g) {
        	//blue
    	    g.setColor(Color.BLUE);
            g.fillOval(30, 30, RING_RADIUS, RING_RADIUS);
            g.setColor(new Color(255, 255, 255, 255));
            g.fillOval(35, 35, INNER_RADIUS, INNER_RADIUS);

            //black
    	    g.setColor(Color.BLACK);
            g.fillOval(90, 30, RING_RADIUS, RING_RADIUS);
            g.setColor(new Color(255, 255, 255, 255));
            g.fillOval(95, 35, INNER_RADIUS, INNER_RADIUS);

            //red
    	    g.setColor(Color.RED);
            g.fillOval(150, 30, RING_RADIUS, RING_RADIUS);
            g.setColor(new Color(255, 255, 255, 255));
            g.fillOval(155, 35, INNER_RADIUS, INNER_RADIUS);

            //yellow
    	    g.setColor(Color.YELLOW);
            g.fillOval(60, 60, RING_RADIUS, RING_RADIUS);
            g.setColor(new Color(255, 255, 255, 255));
            g.fillOval(65, 65, INNER_RADIUS, INNER_RADIUS);

            //green
    	    g.setColor(Color.GREEN);
            g.fillOval(120, 60, RING_RADIUS, RING_RADIUS);
            g.setColor(new Color(255, 255, 255, 255));
            g.fillOval(125, 65, INNER_RADIUS, INNER_RADIUS);
        }
    }

    public static void main(String[] args) {
        JFrame ring = new olympicRing();
        ring.setTitle("Olympic Ring");
        ring.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        ring.setVisible(true);

        System.out.println(ring);
    }
}