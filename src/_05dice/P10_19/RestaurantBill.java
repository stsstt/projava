package P10_19;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class RestaurantBill extends JFrame {
	public static final int FRAME_WIDTH = 300;
	public static final int FRAME_HEIGHT = 200; 

	//instance variables
	private JButton sushi;
    private JButton ramen;
    private JButton friedRice;
    private JButton kyoza;
    private JButton padThai;
    private JButton thaiTea;
    private JButton dumplings;
    private JButton udon;
    private JButton finalizeButton;
    
    private JLabel priceLabel;
    private JTextField priceField;
    private JPanel panel;
    
    private double bill;	//total price
    
    //Constructor
    public RestaurantBill() {
        this.createComponent();
        this.createPanel();
        this.setSize(FRAME_WIDTH, FRAME_HEIGHT);
    }

    private void createComponent() {
        this.createLabel();
        this.createTextField();
        this.createButton();
    }

    private void createLabel() {
        this.priceLabel = new JLabel("Price: ");
    }

    private void createTextField() {
        this.priceField = new JTextField(5);
    }

    //buttons
    private void createButton() {
        class OrderListener implements ActionListener {
            private double price;
            public OrderListener(double price) {
                this.price = price;
            }
            public void actionPerformed(ActionEvent e) {
                bill += this.price;
            }
        }        
        this.sushi = new JButton("Sushi");
        this.sushi.addActionListener(new OrderListener(18));
        this.ramen = new JButton("Ramen");
        this.ramen.addActionListener(new OrderListener(9.5));
        this.friedRice = new JButton("Fried Rice");
        this.friedRice.addActionListener(new OrderListener(11));
        this.kyoza = new JButton("Kyoza");
        this.kyoza.addActionListener(new OrderListener(8));
        this.padThai = new JButton("Pad Thai");
        this.padThai.addActionListener(new OrderListener(13));
        this.thaiTea = new JButton("Thai Tea");
        this.thaiTea.addActionListener(new OrderListener(3.5));
        this.dumplings = new JButton("Dumplings");
        this.dumplings.addActionListener(new OrderListener(6));
        this.udon = new JButton("Udon");
        this.udon.addActionListener(new OrderListener(10));

        this.finalizeButton = new JButton("Finalize Button!");
        this.finalizeButton.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		String billToString = String.format("%.1f", bill);
        		priceField.setText(billToString);
            }
        });
    }

    //panel
    private void createPanel() {
        this.panel = new JPanel();
        this.panel.add(this.sushi);
        this.panel.add(this.ramen);
        this.panel.add(this.friedRice);
        this.panel.add(this.kyoza);
        this.panel.add(this.padThai);
        this.panel.add(this.thaiTea);
        this.panel.add(this.dumplings);
        this.panel.add(this.udon);
        this.panel.add(this.finalizeButton);
        this.panel.add(this.priceLabel);
        this.panel.add(this.priceField);
        this.add(panel);
    }

    public static void main(String[] args) {
    	JFrame frame = new RestaurantBill();    	
        frame.setTitle("Restaurant Bill");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
        
        System.out.println(frame);
    }
}	