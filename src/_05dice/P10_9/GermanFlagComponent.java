package P10_9;

import java.awt.Color;
import java.awt.Graphics
import javax.swing.JComponent;

public class GermanFlagComponent extends JComponent {
	public void paintComponent(Graphics g) {
		drawGermanFlag(g, 10, 125, 200, 50);
	}
	
	//Black - Red - Yellow
	public void drawGermanFlag(Graphics g, int x, int y, int width, int height) {	//added an extra parameter for height
		g.setColor(Color.YELLOW); 
		g.fillRect(x, y, width, height);	
		
		g.setColor(Color.RED);
		g.fillRect(x, y - height, width, height);

		g.setColor(Color.BLACK);
		g.fillRect(x, y - height*2, width, height);

	}
}