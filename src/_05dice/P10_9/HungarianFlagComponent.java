package P10_9;

import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JComponent;

public class HungarianFlagComponent extends JComponent {
	public void paintComponent(Graphics g) {
		drawHungarianFlag(g, 10, 125, 200, 50);
	}
	
	//Red - White - Green
	public void drawHungarianFlag(Graphics g, int x, int y, int width, int height) {
		g.setColor(Color.RED);
		g.fillRect(x, y - height*2, width, height);
		
		g.setColor(Color.WHITE);
		g.fillRect(x, y - height, width, height);

		g.setColor(Color.GREEN); 
		g.fillRect(x, y, width, height);	
		
	}	
}