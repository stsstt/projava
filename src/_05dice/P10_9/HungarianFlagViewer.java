package P10_9;

import javax.swing.JComponent;
import javax.swing.JFrame;

public class HungarianFlagViewer {

	public static void main(String[] args) {
		JFrame frame = new JFrame();
		frame.setSize(300, 400);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		JComponent component = new HungarianFlagComponent();
		frame.add(component);
		frame.setVisible(true);
	}
}