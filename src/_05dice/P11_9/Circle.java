package P11_9;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;

class Circle {
    private Point center;
    private double radius;
    private Color color;

    public Circle(Point center, double radius, Color color) {
        this.center = center;
        this.radius = radius;
        this.color = color;
    }

    public void draw(Graphics g){
        g.setColor(color);
        //drawOval starts to draw at the top-left corner, so we must subtract the raduis from center-point to adjust
        g.drawOval((int)(center.getX() - radius), (int) (center.getY() -radius), (int) (2*radius), (int)(2*radius));
    }
}