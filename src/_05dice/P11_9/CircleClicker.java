package P11_9;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class CircleClicker {
    private JPanel mPanel;
    private Point start, end;

    public static void main(String[] args) {
    	JFrame frame = new JFrame("CircleClicker");
        frame.setContentPane(new CircleClicker().mPanel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
        frame.setSize(new Dimension(800, 600));
    }

    public CircleClicker() {

    	mPanel.addMouseListener(new MouseAdapter() {
            public void mousePressed(MouseEvent e) {
    			start = e.getPoint();
            }
            public void mouseReleased(MouseEvent e) {
            	end = e.getPoint();
                double verticalLeg = Math.abs(end.y - start.y);
                double horizontalLeg = Math.abs(end.x - start.x);
                double hypotenus = Math.hypot(verticalLeg, horizontalLeg);
                Circle circle = new Circle(start, hypotenus, Color.BLACK);
                circle.draw(mPanel.getGraphics());
            }
        });
    }
}