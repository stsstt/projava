package E12_4;

import java.util.*;

public class LevelTwo {

	int score;
	int tries;
	int answer;
	int x;
	int y;
	
	public LevelTwo() {
		this.score = 0;
		this.tries = 0;
		this.answer = 0;
		this.x = 0;
		this.y = 0;
	}

	public int getUserAns() {
		Scanner sc = new Scanner(System.in);
		return sc.nextInt();
	}
	
	public void ask() {
		x = (int) (Math.random() * 10);
		y = (int) (Math.random() * 10);
		while (x==0 || y==0) {	//continue getting random ints for x and y until the conditions are met
			x = (int) (Math.random() * 10);
			y = (int) (Math.random() * 10);
		}	
		System.out.println(x +" + " + y + " = ?");
		this.answer = x + y;
		
		if (this.getUserAns() == this.answer) {
			this.addScore();
			System.out.println("Correct (Score:" + this.score + ", " + this.tries + "/3 attempts)");
		}else {
			this.addTries();
			System.out.println("Incorrect (Score:" + this.score + ", " + this.tries + "/3 attempts)");
		}
	}
	
	public int getAnswer() {
		return this.answer;
	}
	
	public int getTries() {
		return this.tries;
	}
	
	public void addTries() {
		this.tries++;
	}
	
	public int getScore() {
		return this.score;
	}
	
	public void addScore() {
		this.score ++;
	}
}