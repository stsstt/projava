package E12_4;

public class driver {

	public static void main(String[] args) {

		LevelOne l1 = new LevelOne();
		LevelTwo l2 = new LevelTwo();
		LevelThree l3 = new LevelThree();
		
		System.out.println("Start with Level 1!\n");
		
		while (l1.getTries()<3 && l1.getScore()<5) {
			l1.ask();
		}
		if (l1.getTries()==3) {
			System.out.println("\nToo many incorrect answers!\n");
			return;
		}else {	//l1.getScore()==5
			System.out.println("\nMove on to Level 2!\n");
			while (l2.getTries()<3 && l2.getScore()<5) {
				l2.ask();
			}
			if (l2.getTries()==3) {
				System.out.println("\nToo many incorrect answers!\n");
				return;
			}else {	//l2.getScore()==5
				System.out.println("\nMove on to Level 3!\n");
				while (l3.getTries()<3 && l3.getScore()<5) {
					l3.ask();
				}
				if (l2.getTries()==3) {
					System.out.println("\nToo many incorrect answers!\n");
					return;
				}else {	//l3.getScore()==5
					System.out.println("\nSuccessful game!");
				}
			}
		}
	}
}