package P12_1;

public class Coin {
	
	private String coin;	//only accept penny, nickel, dime, quarter
	private int count;	//number of each kind of coin
	
	public Coin(String coin, int count) {
		this.coin = coin;
		this.count = count;
	}
	
	public String getCoinType() {
		return this.coin;
	}
	
	public double getSum() {
		if (this.coin == "penny")
			return 0.01 * this.count;
		else if (this.coin == "nickel")
			return 0.05 * this.count;
		else if (this.coin == "dime")
			return 0.1 * this.count;
		else if (this.coin == "quarter")
			return 0.25 * this.count;
		else
			return 0.00;	//coin is none of the above
	}
	
	public String toString() {
		return this.coin + " (" + this.count + ")";
	}
}
