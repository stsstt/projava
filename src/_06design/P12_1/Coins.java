package P12_1;

public class Coins {

	private Coin[] coins;
	
	public Coins(Coin[] coins) {
		this.coins = coins;
	}
	
	public double getTotalSum() {
		double sum = 0;
		for (Coin coin : this.coins) {
			sum += coin.getSum();
		}
		return sum;
	}
	
	public String toString() {
		return "Total: $" + this.getTotalSum();
	}
}
