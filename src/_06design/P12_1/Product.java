package P12_1;

public class Product {
	
	private String name;	//product name
	private double price;	//product price
	
	public Product(String name, double price) {
		this.name = name;
		this.price = price;
	}
	
	public String getName() {
		return this.name;
	}
	
	public double getPrice() {
		return this.price;
	}
	
	public String toString() {
		return this.getName() + "($" + this.getPrice() + ")";
	}

}
