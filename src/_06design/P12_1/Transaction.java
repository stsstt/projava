package P12_1;

public class Transaction {

	private VendingMachine vendingMachine;
	private User user;
	
	public Transaction(VendingMachine vm, User user) {
		this.vendingMachine = vm;
		this.user = user;
	}
	
	public boolean isSuccess() {	//is transaction successful
		if (!this.vendingMachine.hasProduct(user.getSelect())) {	//product is out of stock
			return false;
		}else {	//check if user has enough coins to cover the price
			if (this.user.getSelect().getPrice() <= this.user.getInput()) {
				return true;
			}
			return false;
		}	
	}
	
	public String toString() {
		if (this.isSuccess())
			return this.user + "\n(Transaction success, claim your product)";
		else
			return this.user + "\n(Transaction fail, not enough coins)";
	}
}