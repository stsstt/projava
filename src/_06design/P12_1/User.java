package P12_1;

public class User {

	private Product product;
	private Coins coins;
	
	public User(Product product, Coins coins) {
		this.product = product;
		this.coins = coins;
	}
		
	public Product getSelect() {
		return this.product;
	}
	
	public double getInput() {
		return this.coins.getTotalSum();
	}
	
	public String toString() {
		return "Selected: " + this.getSelect() + "\nPaid: $" + this.getInput(); 
	}
}
