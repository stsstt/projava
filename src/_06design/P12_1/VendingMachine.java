package P12_1;

public class VendingMachine {

	private Product[] products;	//products that the vending machine currently holds
	
	public VendingMachine(Product[] products) {
		this.products = products;
	}

	public boolean hasProduct(Product prod) {
		for (Product product : this.products) {
			if (product.equals(prod))	//in stock
				return true;
		}
		return false;	//out of stock
	}
	
	public void restock(Product[] addProducts) {
		int curr = this.products.length;
		int add = addProducts.length;
		Product[] newStock = new Product[curr + add];
		int j = 0;
		for (int i=0; i<curr; i++)
			newStock[i] = this.products[j++];
		int k = 0;
		for (int i=curr; i<curr+add; i++)
			newStock[i] = addProducts[k++];
	}
	
	//Show what the vending machine currently holds
	public String toString() {
		String stock = "";
		for (int i=0; i<this.products.length-1; i++)
			stock += products[i] + "\n";
		stock += products[products.length-1];
		return "[Vending Machine]\n" + stock;
	}
}

