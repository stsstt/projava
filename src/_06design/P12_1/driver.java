package P12_1;

public class driver {

	public static void main(String[] args) {

		Coin p = new Coin("penny", 5);
		Coin n = new Coin("nickel", 3);
		Coin d = new Coin("dime", 4);
		Coin q = new Coin("quarter", 7);
		Coins coins = new Coins(new Coin[] {p,n,d,q});
		Product p1 = new Product("Cheetos", 0.85);
		Product p2 = new Product("Doritos", 0.95);
		Product p3 = new Product("Nachos", 1.0);
		Product p4 = new Product("Cheese Puffs", 0.75);
		Product p5 = new Product("Payday", 1.2);
		Product p6 = new Product("Twix", 1.15);
		Product p7 = new Product("Three Musketeers", 1.85);
		Product[] stock = new Product[] {p1, p2, p3, p4, p5, p6, p7};
		
		User user = new User(p7, coins);
		
		VendingMachine vm = new VendingMachine(stock);
		
		Transaction t = new Transaction(vm, user);
		
		System.out.println(p);
		System.out.println(n);
		System.out.println(d);
		System.out.println(q);
		System.out.println(coins);
		System.out.println(p1);
		System.out.println(user);
		System.out.println(vm);
		System.out.println("\n"+t);
		
	}

}