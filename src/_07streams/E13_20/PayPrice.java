package E13_20;

import java.util.*;

public class PayPrice {

	public static void main(String[] args) {
		System.out.println(payPrice(28));
	}
	
	public static HashSet<List<Integer>> payPrice(int n) {
		HashSet<List<Integer>> set = new HashSet<List<Integer>>();	//use set to keep out duplicates
		List<List<Integer>> list = new ArrayList<>();	//will hold all combinations regardless of duplicity

		combination(list, new ArrayList<>(), n, n);
    	
    	for (List<Integer> l : list) {
    		Collections.sort(l);
    		if (!set.contains(l)) {	//don't add if the same combination has already been added
    			set.add(l);
    		}
    	}
    	return set;
	}

	//recursive helper method
    private static void combination(List<List<Integer>> list, List<Integer> temp, int n, int remain) {
    	if (remain == 0) {
    		list.add(new ArrayList<>(temp));
    	}else {
    		if (remain >= 1) {
		    	temp.add(1);
		    	combination(list, temp, n, remain - 1);
				temp.remove(temp.size()-1);
		    }if (remain >= 5) {
				temp.add(5);
		    	combination(list, temp, n, remain - 5);
				temp.remove(temp.size()-1);
			}if (remain >= 20) {
				temp.add(20);
		    	combination(list, temp, n, remain - 20);
				temp.remove(temp.size()-1);
			}if (remain >= 100) {
				temp.add(100);
		    	combination(list, temp, n, remain - 100);
				temp.remove(temp.size()-1);
			}
    	}
	}
}