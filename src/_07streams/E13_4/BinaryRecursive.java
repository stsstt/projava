package E13_4;

public class BinaryRecursive {

	public static void main(String[] args) {
		System.out.println(binary(4));
		System.out.println(binary(9));
		System.out.println(binary(13));
	}
	
	public static String binary(int n) {
		if (n==1)
			return "1";
		if (n==0)
			return "0";
		return binary(n/2) + (n % 2 == 0 ? "0" : "1");
	}
}