package E19_14;

import java.io.*;
import java.nio.file.*;
import java.util.*;
import java.util.stream.*;

public class E19_14 {

	public static void main(String[] args) throws IOException {

	    Stream<String> lines = Files.lines(Paths.get("words.txt"));
	 
	    Optional<String> palindrome = lines
	    		.map(w -> w.replace("'", ""))	//get rid of apostrophes
				.map(w -> w.replace("ö", "O"))	//change these characters to alphabets
				.map(w -> w.replace("é", "E"))
				.map(w -> w.replace("Å", "A"))
				.filter(w -> w.length() >= 5)	//filter out words with at least five letters
				.map(String::toUpperCase)	//convert to uppercase before testing whether is palindromic
				.filter(E19_14::isPalindrome)	//is palindromic?
				.findAny();	//return any palindrome
	 
        System.out.println(palindrome.get());
	 
	    lines.close();
	}
	//program will produce the same word every time
	
	public static boolean isPalindrome(String s) {	//helper function to determine if a string is palindromic
		int i=0, j=s.length()-1;
		while (i <= j) {
			if (s.charAt(i) != s.charAt(j))
				return false;
			i++;
			j--;
		}
		return true;
	}
	
}