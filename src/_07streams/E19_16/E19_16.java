package E19_16;

import java.io.*;
import java.nio.file.*;
import java.util.*;
import java.util.stream.*;

public class E19_16 {

	public static void main(String[] args) throws IOException {
		
		Stream<String> words = Files.lines(Paths.get("words.txt"));
		
		Map<String, Double> averageInitial = words
				.collect(Collectors.groupingBy(s -> s.substring(0, 1).toUpperCase(),
						Collectors.averagingInt(s -> s.length())));
			
		System.out.println(averageInitial);
		words.close();
	}
}