package E19_5;

import java.util.stream.*;

public class E19_5 {

	public static void main(String[] args) {
		Stream<String> words = Stream.of("how much wood could a wood chuck chuck".split(" "));
		System.out.println(toString(words, 4));
		
	}
	
	public static <T> String toString(Stream<T> stream, int n) {

		return stream
				.limit(n)	//limit to first n elements
				.map(s -> s.toString())	//convert to Strings
				.collect(Collectors.joining(","));	//joining by a comma
		
	}
	
}