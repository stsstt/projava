package E19_6;

import java.util.Currency;
import java.util.Set;
import java.util.stream.*;

public class E19_6 {

	public static void main(String[] args) {

		Set<Currency> currencies = Currency.getAvailableCurrencies();
		Stream<Currency> currStream = currencies.stream();
		
		currStream
		.map(Currency::getDisplayName)
		.sorted()
		.forEach(System.out::println);
		
	}
}