package E19_7;

import java.util.stream.*;
import java.util.*;

public class E19_7 {

	public static void main(String[] args) {
		
		List<String> s = new ArrayList<>();
		s.add("Java Class");
		s.add("Sarah Kim");
		s.add("Students");
		System.out.println(toDots(s));
	}

	public static List<String> toDots(List<String> s) {
		
		Stream<String> str = s.stream();
		return str
				.filter(w -> w.length() >= 2)	//filter by length
				.map(w -> {	//make string using first and last letter and 3 dots
					return w.charAt(0) + "..." + w.charAt(w.length()-1);
				})
				.collect(Collectors.toList());	//make list
	}
	
}