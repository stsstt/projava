package P13_3;

import java.util.*;

public class P13_1 {

	/*
		P13.1
		Phone numbers and PIN codes can be easier to remember when you find words that spell out the number on a standard phone pad.
		For example, instead of remembering the combination 5282, you can just think of JAVA.
		Write a recursive method that, given a number, yields all possible spellings (which may or may not be real words).
	*/
	
	public static void main(String[] args) {

		System.out.println(allStrings("2345"));
		
	}
	
	public static List<String> allStrings(String number) { 	//n is a four-digit number e.g. PIN number
		
		List<String> list = new ArrayList<>();

		combination(list, new StringBuilder(), number, number);
		
		return list;
	}

	//recursive helper method
	 private static void combination(List<String> list, StringBuilder sb, String number, String remain) {
		 
			List<List<String>> numbers = new ArrayList<>();
			numbers.add(Arrays.asList("0"));				//0
			numbers.add(Arrays.asList("1"));				//1
			numbers.add(Arrays.asList("A", "B", "C"));		//2
			numbers.add(Arrays.asList("D", "E", "F"));		//3
			numbers.add(Arrays.asList("G", "H", "I"));		//4
			numbers.add(Arrays.asList("J", "K", "L"));		//5
			numbers.add(Arrays.asList("M", "N", "O"));		//6
			numbers.add(Arrays.asList("P", "Q", "R", "S"));	//7
			numbers.add(Arrays.asList("T", "U", "V"));		//8
			numbers.add(Arrays.asList("W", "X", "Y", "Z"));	//9
		 
		 if ((remain.length() == 0)) {
			 list.add(new StringBuilder(sb).toString());
		 }else {
			 int i=0;
			 List<String> num = numbers.get(Integer.valueOf(remain.substring(i, i+1)));
			 for (int j=0; j<num.size(); j++) {
				 sb.append(num.get(j));
				 combination(list, sb, number, remain.substring(1));
				 sb.deleteCharAt(sb.length()-1);
			 }
			 i++;
		 }
	 }
}