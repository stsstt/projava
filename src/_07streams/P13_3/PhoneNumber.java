package P13_3;

import java.util.*;
import java.io.*;

public class PhoneNumber {	//P13.3

/*
	P13.3
	With a longer number, you may need more than one word to remember it on a phone pad.
	For example, 263-346-5282 is CODE IN JAVA.
	Using your work from Exercise P13.2, write a program that, given any number, lists all word sequences that spell the number on a phone pad.
*/
	
	public static void main(String[] args) throws FileNotFoundException {

		System.out.println(numbersToWords("2273369")); //2777333363

	}

	public static List<String> numbersToWords(String number) throws FileNotFoundException {
		
		List<String> list = new ArrayList<String>();
		combination(list, new StringBuilder(), onlyDigits(number), onlyDigits(number));
		
		return list;
	}
	
	//recursive helper method
	public static void combination(List<String> list, StringBuilder sb, String number, String remain) throws FileNotFoundException {
				
		List<List<String>> numbers = new ArrayList<>();
		numbers.add(Arrays.asList("0"));				//0
		numbers.add(Arrays.asList("1"));				//1
		numbers.add(Arrays.asList("A", "B", "C"));		//2
		numbers.add(Arrays.asList("D", "E", "F"));		//3
		numbers.add(Arrays.asList("G", "H", "I"));		//4
		numbers.add(Arrays.asList("J", "K", "L"));		//5
		numbers.add(Arrays.asList("M", "N", "O"));		//6
		numbers.add(Arrays.asList("P", "Q", "R", "S"));	//7
		numbers.add(Arrays.asList("T", "U", "V"));		//8
		numbers.add(Arrays.asList("W", "X", "Y", "Z"));	//9
		
		File file = new File("words.txt");
		Scanner sc = new Scanner(file);
		
		while (sc.hasNext()) {
			
	    	if (remain.length() == 0) {
	    		list.add(new StringBuilder(sb).toString());
	    	}else {
				String str = sc.next();
				String s = onlyString(str);
				int i=0;
	    		if (s.length() <= remain.length() && s.length()>=2) {
	    			while (i < s.length() && numbers.get(Integer.valueOf(number.substring(i, i+1))).contains(s.substring(i, i+1))) {
	    				i++;
	    			}
	    		}
	    		if (i==s.length()) {
	    			sb.append(str);
	    			combination(list, sb, number, remain.substring(i));
	    			sb.deleteCharAt(sb.length()-1);
	    		}
	    	}
		}
    	sc.close();
	}
	
	public static String onlyDigits(String s) {	//returns string without non-digit characters
		StringBuilder sb = new StringBuilder();
		for (char c : s.toCharArray()) {
			if (Character.isDigit(c)) {
				sb.append(c);
			}
		}
		return sb.toString();
	}
	
	public static String onlyString(String s) {	//returns the word in upper case alphabets only
		StringBuilder sb = new StringBuilder();
		for (char c : s.toCharArray()) {
			if (Character.isAlphabetic(c)) {
				if (c=='Å') sb.append('A');
				else if (c=='ö') sb.append('O');
				else if (c=='é') sb.append('E');
				else sb.append(c);
			}
		}
		return sb.toString().toUpperCase();
	}
}