package yoda;

public class YodaSpeak {	//iterative
	
	public static void main(String[] args) {

		System.out.println(YodaIter("The force is strong with you"));
	
	}

	public static String YodaIter(String s) {

		String[] array = s.split(" ");	//split sentence into words
		int n = array.length;	//count the number of words in sentence
		int low = 0, high = n - 1;

		while (low <= high) {	//iterate up to the middle
			String temp = array[low];	//swap words on the two ends
			array[low] = array[high];
			array[high] = temp;
			low++;
			high--;
		}
		
		StringBuilder sb = new StringBuilder();
		for (String str : array) {
			sb.append(str);
			sb.append(" ");
		}
		sb.deleteCharAt(sb.length()-1);	//delete the extra space at the end
		
		return sb.toString();
		
	}
	
}