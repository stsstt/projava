package yoda;

public class YodaSpeakRecursive {	//recursive

	public static void main(String[] args) {

		System.out.println(YodaRecur("The force is strong with you"));
	
	}

	public static String YodaRecur(String s) {

		String[] array = s.split(" ");	//split sentence into words
		
		if (array.length==0 || array.length==1)
			return s;
		
		int first = -1;	//index of first space
		for (int i=0; i<s.length(); i++)
			if (s.charAt(i)==' ') {
				first = i;
				break;
			}
		
		int last = -1;	//index of last space
		for (int j=s.length()-1; j>=first; j--) {
			if (s.charAt(j)==' ') {
				last = j;
				break;
			}
		}
		
		System.out.println("first " + first + " last " + last);
		if (first == last)	//there is one space left between two words
			return s.substring(first+1, s.length()) + " " + s.substring(0,first);
		
		int low = 0, high = array.length - 1;
		return array[high] + " " + YodaRecur(s.substring(first+1, last)) + " " + array[low];
	
	}
}