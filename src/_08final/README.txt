1. Increased frequency of floaters

2. Set debris color to change randomly every time

3. Keep scoring: +5 pts for large, +2 pts for medium, +1 pt for small asteroids

4. Increase the number of falcons by 1 when collision with a floater

5. Change font - style, size

6. Track the highest score attained so far and show on the upper right corner of the frame, show in WHITE
- maintained until window closing, or Quit

7. Modify drawScore method to show current level together with the score, show on the upper left corner, show both in CYAN

8. Show the last available falcon as YELLOW