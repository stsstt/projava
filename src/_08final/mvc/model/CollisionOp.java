package _08final.mvc.model;

/**
 * Created by ag on 6/17/2015.
 */

public class CollisionOp {
    public enum Operation {
        ADD, REMOVE
    }

    //members
    private Movable mMovable;
    private Operation mOperation;

    //constructor
    public CollisionOp(Movable movable, Operation op) {
        mMovable = movable;
        mOperation = op;
    }

    //getters
    public Movable getMovable() {
        return mMovable;
    }

    public Operation getOperation() {
        return mOperation;
    }
}