package _08final.mvc.model;

import _08final.mvc.controller.Game;

import java.awt.*;
import java.util.*;

public class Debris extends Sprite {

    private int nSpin;

    //radius of a large asteroid
    private final int RAD = 10;
    private final int MAX_EXPIRE = 5;

    //randomize color of debris
    private final Random randomGenerator = new Random();
    private final int red = randomGenerator.nextInt(256);
    private final int green = randomGenerator.nextInt(256);
    private final int blue = randomGenerator.nextInt(256);
    private final Color color = new Color(red,green,blue);

    public Debris(Asteroid astExploded){

        //call Sprite constructor
        super();
        setTeam(Team.DEBRIS);
        int  nSizeNew =	astExploded.getSize() + 1;

        //the spin will be either plus or minus 0-9
        int nSpin = Game.R.nextInt(10);
        if(nSpin %2 ==0)
            nSpin = -nSpin;

        //random delta-x
        int nDX = Game.R.nextInt(10 + nSizeNew*2);
        if(nDX %2 ==0)
            nDX = -nDX;
        setDeltaX(nDX);

        //random delta-y
        int nDY = Game.R.nextInt(10+ nSizeNew*2);
        if(nDY %2 ==0)
            nDY = -nDY;
        setDeltaY(nDY);

        assignRandomShape();

        //an nSize of zero is a big asteroid
        //a nSize of 1 or 2 is med or small asteroid respectively

        setRadius(RAD/(nSizeNew * 2));
        setCenter(astExploded.getCenter());
        setExpire(MAX_EXPIRE);
        setColor(color);
    }

    @Override
    public void move() {
        super.move();
        if (getExpire() < MAX_EXPIRE -5){
            setDeltaX(getDeltaX() * 1.07);
            setDeltaY(getDeltaY() * 1.07);
        }
        if (getExpire() == 0)
            CommandCenter.getInstance().getOpsList().enqueue(this, CollisionOp.Operation.REMOVE);
        else
            setExpire(getExpire() - 1);
    }

    public void assignRandomShape ()
    {
        int nSide = Game.R.nextInt( 7 ) + 7;
        int nSidesTemp = nSide;

        int[] nSides = new int[nSide];
        for ( int nC = 0; nC < nSides.length; nC++ )
        {
            int n = nC * 48 / nSides.length - 4 + Game.R.nextInt( 8 );
            if ( n >= 48 || n < 0 )
            {
                n = 0;
                nSidesTemp--;
            }
            nSides[nC] = n;
        }
        Arrays.sort(nSides);

        double[]  dDegrees = new double[nSidesTemp];
        for ( int nC = 0; nC <dDegrees.length; nC++ )
        {
            dDegrees[nC] = nSides[nC] * Math.PI / 24 + Math.PI / 2;
        }
        setDegrees( dDegrees);

        double[] dLengths = new double[dDegrees.length];
        for (int nC = 0; nC < dDegrees.length; nC++) {
            if(nC %3 == 0)
                dLengths[nC] = 1 - Game.R.nextInt(40)/100.0;
            else
                dLengths[nC] = 1;
        }
        setLengths(dLengths);
    }
}